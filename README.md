Ongisline
=========

[![Build status](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/build.svg)](https://gitlab.com/gismala/ongisline/commits/master)

The purpose for this project is to build up a Web GIS application and to learn the implementation of GIS tools.

The work is in progress at the moment.

### Installing
The easiest way to try Ongisline on your own machine is with Docker and docker-compose:
```
$ docker-compose up
```
