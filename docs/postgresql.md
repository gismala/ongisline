# Postgresql (and PostGIS) specifig documentation

## PostgreSQL installation
#### Postgres client
If you want to just install postgres-client type:
```
sudo apt-get install postgresql-client
```
You can connect to the database server using:
```
psql -p portnumber -h server.domain.org database user
```
The Pgadmin3 can be installed with just:
```
sudo apt-get install pgadmin3
```
Tip: You can save ip addresses to a name to the /etc/hosts with:
```
000.000.000.00  name
```
#### Postgres (and PostGIS) server
Add repository to sources.list (replace xenial with correct version):
```
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt xenial-pgdg main" >> /etc/apt/sources.list'
sudo apt-get update
```
Install postgresql 9.6, postgis 2.3 by typing:
```
sudo apt-get install postgresql-9.6-postgis-2.3
```

## Configuration

#### New database
Create a new database and user *newuser*:
```
sudo -u postgres psql
```
```
CREATE DATABASE gisdb;
CREATE USER newuser WITH PASSWORD 'password';
GRANT ALL PRIVILEGES ON DATABASE gisdb TO newuser;
\connect gisdb;

CREATE EXTENSION postgis;
SELECT postgis_full_version();
```

#### Allow connections from other hosts
Modify the listen_addresses line in **/etc/postgresql/9.6/main/postgresql.conf**:
```
listen_addresses = '*' # Or write here your the client host
```
Add your db with your user to the **/etc/postgresql/9.6/main/pg_hba.conf**:
```
# TYPE  DATABASE        USER            ADDRESS                 METHOD 
local   all             newuser                                  md5 
host    all             newuser         0.0.0.0/0                md5
```
Restart the service with `sudo service postgresql restart`

#### Performance tuning
From **/etc/postgresql/9.6/main/postgresql.conf**: 
```
shared_buffers = 512MB # 25% of RAM - the most important performance tuner
work_mem = 50MB # To increase the work memory from the default 4MB
max_parallel_workers_per_gather = 4 # Check the CPU cores of your server
# Vacuuming
autovacuum = on
autovacuum_max_workers = 5 # or 6, default 3
maintenance_work_mem = 128MB #Default 64MB - should be RAM * 3 /(8*autovacuum_max_workers)
autovacuum_scale_factor = 0.1 # default 0.2, should be smaller if big tables
```
## Bacup and restore
Restore custom format with (maker sure you have created postgis extension first, since the backup might not contain it):
```
pg_restore -h hostname -U newuser -c -O dump.custom
```
