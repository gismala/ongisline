Instructions for special python libraries
========
### Celery
##### Installation
Celery can be installed with pip: `pip install celery`.
You need a message broker in order to user Celery. Install Redis (without env) by:
```
sudo apt-get install rabbitmq-server
```
You can test if everythin works with a little sample file *tasks.py*:

```python
from celery import Celery

app = Celery('tasks', broker='pyamqp://guest@localhost//')

@app.task
def add(x, y):
    return x + y
```
And by running:
```
celery -A tasks worker --loglevel=info
```
##### Saving results
If you want to store results use RPC (RabbitMQ) or with django as django-celery-results:
```
pip install django-celery-results
# Add django_celery_results to installed apps
python manage.py migrate django_celery_results
```
Then add following to the *settings.py*:
```
CELERY_RESULT_BACKEND = 'django-db'
CELERY_RESULT_BACKEND = 'django-cache'
```

In production environment use http://docs.celeryproject.org/en/latest/userguide/daemonizing.html#daemonizing
To test the celery worker in dev environment with Django try:
```
celery -A ongisline worker -l info
```