Server installation
========
### List of usually needed programs
* ufw - firewall
* man - manual of everything
* vim - editor
* pip3 - Python pip3
* screen - GNU screen: virtual windows

### Firewall (UFW)
Enable OpenSSH by typing:
```
sudo ufw allow OpenSSH
```
Next enable the firewall by typing and confirming with `y`:
```
sudo ufw enable
```
Check if OpenSSH is visible from output of:
```
sudo ufw status
```

### Screen
-S session name
-d -r reattach a session
C-a [ - copy mode: select with space, end with space
C-a ] - paste

### Virtualenv
Install new virtualenv by:
```
pip3 install --upgrade pip
pip3 install virtualenv
virtualenv -p python3 envname
```

### PostgreSQL/PostGIS (16.04)
*TODO: pgrouting*
##### Postgres client
If you want to just install postgres-client type:
```
sudo apt-get install postgresql-client
```
You can connect to the database server using:
```
psql -p portnumber -h server.domain.org database user
```
The Pgadmin3 can be installed with just:
```
sudo apt-get install pgadmin3
```
Tip: You can save ip addresses to a name to the /etc/hosts with:
```
000.000.000.00  name
```
##### Postgres server
Add repository to sources.list:
```
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt xenial-pgdg main" >> /etc/apt/sources.list'
sudo apt-get update
```
Install postgresql 9.6, postgis 2.3 by typing:
```
sudo apt-get install postgresql-9.6-postgis-2.3
```

Create a new database:
```
sudo -u postgres psql
```
```
CREATE DATABASE gisdb;
CREATE USER newuser WITH PASSWORD 'password';
GRANT ALL PRIVILEGES ON DATABASE gisdb TO newuser;
\connect gisdb;

CREATE EXTENSION postgis;
SELECT postgis_full_version();
```

### Geodjango
In order to use geodjango with PostGIS, the following packages must be installed: 
```
sudo apt-get install binutils libproj-dev
sudo apt-get install postgresql-server-dev-9.6
```
If you want to install the newest GDAL version and its Python binaries, type:
```
sudo add-apt-repository ppa:ubuntugis/ppa && sudo apt-get update
sudo apt-get install gdal-bin libgdal-dev

#To install gdal python interface with pip:
export CPLUS_INCLUDE_PATH=/usr/include/gdal
export C_INCLUDE_PATH=/usr/include/gdal
pip install gdal

```

You may have to alter the password of the use postgres to match your Django settings:
```
sudo -u postgres psql
ALTER USER postgres PASSWORD 'newPassword';
```