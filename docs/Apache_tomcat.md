Apache and tomcat installation instructions
===========================================

### Apache server
##### Ubuntu 14.04 and Python3
*Instructions applied from the following tutorial: https://www.digitalocean.com/community/tutorials/how-to-serve-django-applications-with-apache-and-mod_wsgi-on-ubuntu-14-04 and instructions from https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/modwsgi/*

First install Apache with mod_wsgi to be able to communicate with Django:
```
sudo apt-get install apache2 libapache2-mod-wsgi-py3
```

Next configure WSGI pass by editing the default virtual host file:
```
sudo vim /etc/apache2/sites-available/000-default.conf
```
First start by adding WSGI configuration parameters. The python options could not be used when using daemon mode (neede in Windows). Put the following inside the `<VirtualHost *:80>`:
```
WSGIScriptAlias / /mnt/c/Users/joona/Dropbox/Koodit/Gismala/ongisline/ongisline/wsgi.py
WSGIDaemonProcess ongisline python-home=/mnt/c/Users/joona/VirtualEnvs/Ubu/gismala/ongisline/ python-path=/mnt/c/Users/joona/Dropbox/Koodit/Gismala/ongisline
WSGIProcessGroup ongisline
```

Then let the Apache access the **wsgi.py** file by giving:
```
<Directory /mnt/c/Users/joona/Dropbox/Koodit/Gismala/ongisline/ongisline>
    <Files wsgi.py>
        Require all granted
    </Files>
</Directory> 
```

Next configure static files. Map alias `/static`to the "static" directory of the project (remember to collectstatic first):

```
Alias /static /mnt/c/Users/joona/Dropbox/Koodit/Gismala/ongisline/staticfiles
<Directory /mnt/c/Users/joona/Dropbox/Koodit/Gismala/ongisline/staticfiles>
    Require all granted                                                         
</Directory>
```

**TODO: Database permissions/authentication??**

Save and close the file and restart Apache by typing:
```
sudo service apache2 restart
```

The Apache logs are saved to the `/var/log/apache2/` if not specified otherwise.

### X-send-file module (Ubuntu 14.04)
Install Apache mod_xsendfile by typing:
```
sudo apt-get install libapache2-mod-xsendfile
```
Then add following to the `/etc/apache2/sites-available/000-default.conf`:
```
XSendFile On
XSendFilePath /tmp
```
Finally restart the Apache server:

```
sudo service apache2 restart
```
### Tomcat server
##### Ubuntu 14.04, Tomcat 7
Make sure you have updated apt-get packages and install Tomcat by typing:
```
sudo apt-get update && apt-get install tomcat7
```
