GEOSERVER
==========

# Installation
## Binary Installation
Install Java 8 JRE (Oracle is preferred, but OpenJDK has been reported to work also):
```
$ sudo apt-add-repository ppa:webupd8team/java
$ sudo apt-get update
$ sudo apt-get install oracle-java8-installer
```
Check that it installed properly with:  `java -version`.

Download Geoserver Platform independent binary from http://geoserver.org/release/stable/
and sudo unzip it to the /user/share/geoserver (rename the geoserver.2.1.1 etc folder to geoserver).

Add environmental variable to save the Geoserver location: 
```
$ echo "export GEOSERVER_HOME=/usr/share/geoserver" >> ~/.profile
$ source ~/.profile
```
Make yourself the owner of the folder with:
```
$ sudo chown -R `whoami` /usr/share/geoserver/
```
Start the Geoserver from *bin/startup.sh* and close it from *bin/shutdown.sh*. 
~~Make aliases for those with:~~ (Does not work, repair!)
```
$ echo "alias geoserver-startup='sh /user/share/geoserver/bin/startup.sh'" >> ~/.bashrc
$ echo "alias geoserver-shutdown='sh /user/share/geoserver/bin/shutdown.sh'" >> ~/.bashrc
$ source ~/.bashrc
```

Navigate to `http://localhost:8080/geoserver` after starting Geoserver.