Docker
========
### Installation
##### Fast way (Ubuntu, tested on 14.04) 
Install **Docker** by typing:
```
$ wget -qO- https://get.docker.com/ | sh
```
In able to run Docker as a non-root user, you must add yourself in to a docker group:
```
$ sudo usermod -aG docker `whoami`
```
You can check that it worked by:
```
$ groups `whoami`
```
Log in and out to make the effect.

##### "Proper" way (Ubuntu, tested on 16.04)
Install packages to allow apt to use HTTPS:
```
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
```
Add Docker's official GPG key:
```
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
Verify that the key fingerprint is *9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88*
```
$ sudo apt-key fingerprint 0EBFCD88
```
Set up the stable repository (**amd64**):
```
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```
Update apt and install Docker-ce:
```
$ sudo apt-get update
$ sudo apt-get install docker-ce
```
Verify the install by typing:
```
$ sudo docker run hello-world
```

##### Docker-composer
To use Docker-compposer install it from the Github https://github.com/docker/compose/releases
The installation goes with something like:
```
curl -L https://github.com/docker/compose/releases/download/1.12.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

If you have no permissions to write to the */usr/local/bin*, you can curl it elsewhere and then
sudo move it there.

### Usage
##### Build an image
Build image using:
```
docker build -t imagename path_to_dockerfile_folder
```
##### Bash to container
```
docker exec -it <container_name> bash
```
##### Update the image
If you need update the image, do the changes and then:
```
docker rmi -f imagename
docker build -t imagename path_to_dockerfile_folder
docker rm <tab_to_see_the_number_prefix> old_file
```

#### Remove container
```
docker rm <tab-to-select-container>
```

### Get container ip address
```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <tab-to-select-container>
```