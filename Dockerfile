FROM ubuntu:16.04 
ENV PYTHONUNBUFFERED 1
ENV CELERY_IN_USE 1

# Install GDAL
RUN apt-get update && apt-get install -y software-properties-common \
	&& apt-add-repository ppa:ubuntugis/ppa && apt-get update

# Pip for Python3
RUN	apt-get install -y gdal-bin libgdal-dev python3-pip

# GDAL pip package
RUN pip3 install --upgrade pip \
	&& pip3 install gdal==2.1.3 --global-option=build_ext --global-option="-I/usr/include/gdal"

# Install NC for port listening
RUN apt-get install -y netcat-openbsd
# Install requirements and bind folders
RUN mkdir /config  
ADD /config/requirements.txt /config/ 
RUN pip3 install -r /config/requirements.txt && pip3 install redis
RUN mkdir /src;  
WORKDIR /src  
