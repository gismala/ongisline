from django.contrib.gis.db import models
from django.contrib.gis.gdal.geometries import Polygon, MultiPolygon, MultiLineString, LineString
from django.contrib.gis.gdal import OGRGeometry

# Create your models here.

class Shapefile(models.Model):
    filename = models.CharField(max_length=255)
    srs_wkt = models.TextField()
    geom_type = models.CharField(max_length=50)
    encoding = models.CharField(max_length=20)

    def __str__(self):
        return self.filename

class Attribute(models.Model):
    shapefile = models.ForeignKey(Shapefile)
    name = models.CharField(max_length=255)
    type = models.IntegerField()
    width = models.IntegerField()
    precision = models.IntegerField()

    def __str__(self):
        return "{0} belonging to {1}".format(self.name, self.shapefile)

class Feature(models.Model):
    shapefile = models.ForeignKey(Shapefile)
    geom_point = models.PointField(srid=4326, blank=True, null=True)
    geom_multipoint = models.MultiPointField(srid=4326, blank=True, null=True)
    geom_multilinestring = models.MultiLineStringField(srid=4326, blank=True, null=True)
    geom_multipolygon = models.MultiPolygonField(srid=4326,blank=True, null=True)
    geom_geometrycollection = models.GeometryCollectionField(srid=4326,blank=True,null=True)
    objects = models.GeoManager()

    #def save(self, *args, **kwargs):
    #    print("Saving")
    #    if self.geom_multipolygon:
    #        if isinstance(self.geom_multipolygon, Polygon):
    #            geom = OGRGeometry("MULTIPOLYGON")
    #            geom.add(self.geom_multipolygon)
    #            self.geom_multipolygon =  geom.geos
    #    elif self.geom_multilinestring: 
    #        if isinstance(self.geom_multilinestring, LineString):
    #            geom = OGRGeometry("MULTILINESTRING")
    #            geom.add(self.geom_multilinestring)
    #            self.geom_multilinestring =  geom.geos
    #    super(Feature, self).save(*args, **kwargs)

    def __str__(self):
        return "{0} geometry from the {1}".format(self.id, self.shapefile)

    


class AttributeValue(models.Model):
    feature = models.ForeignKey(Feature)
    attribute = models.ForeignKey(Attribute)
    value = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return "{0} : {1}".format(self.value, self.attribute)