from django.contrib.gis.gdal import DataSource, CoordTransform, SpatialReference, Driver, OGRGeometry, GDALRaster
from django.contrib.gis.gdal.field import OGRFieldTypes
#from django.contrib.gis.geos.geometry import GEOSGeometry
from django.contrib.gis.gdal.geometries import Polygon, MultiPolygon, MultiLineString, LineString
#from django.contrib.gis.geos import Polygon, MultiPolygon, MultiLineString, LineString

from django.forms.models import model_to_dict
from django.http import HttpResponse, FileResponse
from wsgiref.util import FileWrapper
from apps.django_gdalextension import DataSource2
from apps.spatial_io.utils import save_data_to_postgis, export_data_from_postgis



from .models import Shapefile, Attribute, Feature, AttributeValue
import uuid
import os, os.path
import shutil
import tempfile
import zipfile

import logging
logger=logging.getLogger(__name__)

def write_file_to_disk(django_temp_file):
    '''
    Write InMemotyUploadFile or TemporaryUploadedFile to the file system
    '''
    fd,fname = tempfile.mkstemp(suffix=".zip")
    os.close(fd)
    f = open(fname, "wb")
    for chunk in django_temp_file.chunks():
        f.write(chunk)
    f.close()
    return fname

def extract_compressed_file(fname):
    zip = zipfile.ZipFile(fname)
    required_suffixes = [".shp", ".shx", ".dbf", ".prj"]
    has_suffix = {}
    [has_suffix.update({suffix:False}) for suffix in required_suffixes]
    
    # Check suffixes for shapefile format
    for info in zip.infolist():
        extension = os.path.splitext(info.filename)[1].lower()
        if extension in required_suffixes:
            has_suffix[extension] = True
    
    for suffix in required_suffixes:
        if not has_suffix[suffix]:
            zip.close()
            os.remove(fname)
            return "Archive missing required "+suffix+" file.", None
    
    shapefile_name = None
    dst_dir = tempfile.mkdtemp()
    for info in zip.infolist():
        if info.filename.endswith(".shp"):
            shapefile_name = info.filename
        dst_file = os.path.join(dst_dir, info.filename)
        f = open(dst_file, "wb")
        f.write(zip.read(info.filename))
        f.close()
    zip.close()
    return dst_dir, shapefile_name



def import_data_to_postgis(fname, tablename):
    logger.debug("Importing data to postgis")
    dst_dir, shapefile_name = extract_compressed_file(fname)
    if not shapefile_name:
        return dst_dir
    try:
        save_data_to_postgis(os.path.join(dst_dir, shapefile_name), tablename, str(uuid.uuid4()).replace("-","_"), False)
    except:
        logger.exception("Error occurred during adding the data")
    finally:
        shutil.rmtree(dst_dir)


def export_from_postgis(pk):
    return export_data_from_postgis(pk, "GeoJSON")



def import_data_trough_keyvalpairs(fname, character_encoding):
    dst_dir, shapefile_name = extract_compressed_file(fname)
    if not shapefile_name:
        return dst_dir

    # Open the shapefile using OGR
    try:
        datasource = DataSource2(os.path.join(dst_dir, shapefile_name))
        layer = datasource[0]
        shapefileOK = True
    except:
        logger.exception("Failed to open the shapefile")
        shapefileOK = False
    if not shapefileOK:
        os.remove(fname)
        shutil.rmtree(dst_dir)
        return "Not a valid shapefile."

    try:
        for layer in datasource:
            geometry_name = layer.geom_type.name
            geometry_field = calc_geometry_field(geometry_name)
            # Store shapefile
            src_spatial_ref = layer.srs
            shapefile = Shapefile.objects.create(filename=shapefile_name, srs_wkt=str(src_spatial_ref), geom_type=geometry_name, encoding=character_encoding)

            # Store attribute fields
            attributes = []
            for i in range(layer.num_fields):
                field_type = [key for key, val in OGRFieldTypes.items() if val == layer.field_types[i]][0]
                attr = Attribute.objects.create(shapefile=shapefile, name=layer.fields[i], type=field_type, width=layer.field_widths[i], precision=layer.field_precisions[i])
                attributes.append(attr)

            # Store features
            #transform = CoordTransform(src_spatial_ref, SpatialReference(4326))
            for feat in layer:
                #feat.geom.transform(transform)
                args = {}
                args['shapefile'] = shapefile
                args[geometry_field] = wrap_geometry(feat.geom)
                feature = Feature.objects.create(**args)

                # Store attribute values
                for attr in attributes:
                    try:
                        attr_value = feat.get_value(attr.name)
                        attr_value = AttributeValue.objects.create(feature=feature, attribute=attr, value=attr_value)
                    except:
                        logger.exception("Failed to add attribute " + str(attr))
                        os.remove(fname)
                        shutil.rmtree(dst_dir)
                        shapefile.delete()
                        return "Error with attributes"

    except:
        logger.exception("Error occurred")
        if shapefile:
            shapefile.delete()
        try:
            os.remove(fname)
            shutil.rmtree(dst_dir)
        except:
            logger.exception("Couldn't clean the temp folder")
        del datasource
        return 

    try:
        os.remove(fname)
        shutil.rmtree(dst_dir)
    except:
        logger.exception("Couldn't clean the temp folder")
    del datasource
    logger.debug("Finished loading the shapefile")
    return None

def export_data(shapefile, driver="ESRI Shapefile"):
    dst_dir = tempfile.mkdtemp()
    dst_file = str(os.path.join(dst_dir, shapefile.filename))
    ds = None
    layer = None
    try:
        shapeDict = model_to_dict(shapefile)
        shapeDict['filename'] = os.path.join(dst_dir, shapeDict['filename'])
        geom_field = calc_geometry_field(shapeDict['geom_type'])
        ds = DataSource2(dict({"driver":driver}, **shapeDict), ds_create=True, encoding=shapefile.encoding)
        layer = ds.create_layer("testlayer")
        for attrs in shapefile.attribute_set.all():
            layer.create_field(dict({"layer":layer}, **model_to_dict(attrs)))
        
        for feature in shapefile.feature_set.all():
            attrs = feature.attributevalue_set.all()
            geom = getattr(feature, geom_field)
            geom =unwrap_geos_geometry(geom)
            layer.create_feature({"geom": geom.json, "attributes": attrs})
        
        del ds
        del layer

        with tempfile.SpooledTemporaryFile() as temp:
            with zipfile.ZipFile(temp, 'w', zipfile.ZIP_DEFLATED) as zip:
                for fName in os.listdir(dst_dir):
                    zip.write(os.path.join(dst_dir, fName), fName)
            shapefile_base = os.path.splitext(dst_file)[0]
            shapefile_name = os.path.splitext(shapefile.filename)[0]
            shutil.rmtree(dst_dir)

            #if os.name != 'nt': 
            #    response = HttpResponse()
            #    response['Content-Disposition'] = "attachment; filename=" + shapefile_name + ".zip"
            #    response['X-Sendfile'] = "/tmp/{0}".format(dst_file)
            #    return response


            response = FileResponse(temp.read(), content_type="application/zip")
            #response = HttpResponse(temp.read(), content_type="application/zip")
            response['Content-Disposition'] = "attachment; filename=" + shapefile_name + ".zip"
            response['Content-Length'] = temp.tell()
            #temp.seek(0)
        
            return response

    except:
        logger.exception("Error occurred while exporting a shapefile")
        del ds
        del layer
        shutil.rmtree(dst_dir)
        return HttpResponse("Error occurred")
    
 
def wrap_geometry(geometry):
    if isinstance(geometry, Polygon) or isinstance(geometry, MultiPolygon):
        geom = OGRGeometry("MULTIPOLYGON")
        geom.add(geometry)
        return geom.geos
    elif isinstance(geometry, LineString) or isinstance(geometry, MultiLineString):
        geom = OGRGeometry("MULTILINESTRING")
        geom.add(geometry)
        return geom.geos
    return geometry

def wrap_geos_Geometry(geometry):
    if isinstance(geometry, Polygon) or isinstance(geometry, MultiPolygon):
        return MultiPolygon(geometry)
    elif isinstance(geometry, LineString) or isinstance(geometry, MultiLineString):
        return MultiLineString(geometry)
    else:
        return geometry

def unwrap_geos_geometry(geometry):
    if isinstance(geometry, MultiPolygon) or isinstance(geometry, MultiLineString):
        if len(geometry) == 1:
            geometry = geometry[0]
    return geometry
        


def calc_geometry_field(geometry_type):
    if geometry_type == "Polygon":
        return "geom_multipolygon"
    elif geometry_type == "LineString":
        return "geom_multilinestring"
    else:
        return "geom_" + geometry_type.lower()

def getOGRFeatureAttribute(attr, feature, encoding):
    attr_name = str(attr.name)
    #if not feature.IsFieldSet(attr_name):
    #    return (True, None)
    needs_encoding = False
    if attr.type == 0: #OFTInteger
        value = feature[attr_name].as_string()
    elif attr.type == 1: #OFTIntegerList
        value = repr(feature[attr_name].value)
    elif attr.type == 2: #OFTReal
        value = feature[attr_name].as_double()
        value = "%*.*f" % (attr.width, attr.precision, value)
    elif attr.type == 3: #OFTRealList
        values = feature[attr_name].value
        str_values = []
        for value in values:
            str_values.append("%*.*f" % (attr.width,
                            attr.precision,
                            value))
        value = repr(str_Values)
    elif attr.type == 4: #OFTString
        value = feature[attr_name].as_string()
        needs_encoding = True
    elif attr.type == 5: #OFTStringList
        value = repr(feature[attr_name].value)
        needs_encoding = True
    elif attr.type == 9: #OFTDate:
        parts = feature[attr_name].as_datetime()
        year,month,day,hour,minute,second,tzone = parts
        value = "%d,%d,%d,%d" % (year,month,day,tzone)
    elif attr.type == 10: #OFTTime
        parts = feature[attr_name].as_datetime()
        year,month,day,hour,minute,second,tzone = parts
        value = "%d,%d,%d,%d" % (hour,minute,second,tzone)
    elif attr.type == 11: #OFTDateTime
        parts = feature[attr_name].as_datetime()
        year,month,day,hour,minute,second,tzone = parts
        value = "%d,%d,%d,%d,%d,%d,%d,%d" % (year,month,day,
                hour,minute,
                second,tzone)
    else:
        # TODO: implement other fields
        return (False, "Unsupported attribute type: " + str(attr.type))
    if needs_encoding:
        try:
            value = value.encode(encoding)
            value = value.decode(encoding)
        except UnicodeDecodeError:
            logger.exception("Decoding failed")
            return (False, "Unable to decode value in " +
                    repr(attr_name) + " attribute.&nbsp; " +
                    "Are you sure you're using the right " +
                    "character encoding?")
    return (True, value)


def setOGRFeatureAttribute(attr, value, encoding):
    attr_name = str(attr.name)