from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^editor/$', views.list_shapefiles, name='book_example_list_shapefiles'),
    url(r'^editor/import/$', views.import_shapefile, name='book_example_editor_import'),
    url(r'^editor/export/(?P<shapefile_id>\d+)$', views.export_shapefile, name='book_example_editor_export'),
    url(r'^editor/edit/(?P<shapefile_id>\d+)$', views.dummy, name='book_example_editor_edit'),
    url(r'^editor/delete/(?P<shapefile_id>\d+)$', views.delete_shapefile, name='book_example_editor_delete'),
]