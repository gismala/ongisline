from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.datastructures import MultiValueDict
from django.http.response import HttpResponse, Http404, JsonResponse
from django.views.decorators.csrf import csrf_protect

from apps.bookexample.models import Shapefile
from apps.spatial_io.models import Spatialdata
from apps.converter.forms import importFilesForms
from apps.bookexample import tasks, utils
from apps.bookexample.tasks import import_to_postgis

import logging, os
logger=logging.getLogger(__name__)



def list_shapefiles(request):
    context = {}
    if request.method == "GET":
        shapefiles = Spatialdata.objects.all().order_by("name") #Shapefile.objects.all().order_by("filename")
        context['shapefiles'] = shapefiles
    return render(request, "bookexample/list_shapefiles.html", context)

@csrf_protect
def import_shapefile(request):
    if request.is_ajax() and request.method == 'POST':
        errorMsg = ""
        isSuccess = True
        logger.debug("Post and files: {0}, {1}".format(request.POST, request.FILES))
        # If the files are dropped, they are on the files[]. If choosed normally they are in import_file
        importFiles = request.FILES.getlist('import_file') + request.FILES.getlist('files[]')
        cleaned_files = {}
        for importFile in importFiles:
            filesForm = importFilesForms(request.POST, MultiValueDict({'import_file' : [importFile]}))
            if not filesForm.is_valid():
                errorMsg = filesForm.errors['import_file']
                isSuccess = False
                break
            cleaned_files[filesForm.cleaned_data['import_file']] = filesForm.encoding

        if isSuccess:
            logger.debug("upload is valid!")
            for fil, encoding in cleaned_files.items():
                file_name = utils.write_file_to_disk(fil)
                name = fil.name[:-4]
                if "CELERY_IN_USE" not in os.environ:
                    tasks.import_to_postgis(file_name, name) #tasks.call_import_data(file_name, encoding)
                else:
                    import_to_postgis.delay(file_name, name) #tasks.call_import_data.delay(file_name, encoding)
        return JsonResponse({"upload_success": isSuccess, "upload_error" : errorMsg})

    elif request.is_ajax() and request.method == 'GET':
        logger.debug("GET: {0}".format(request.GET))
        return render(request, "bookexample/import_shapefiles.html", 
                        {"form" : importFilesForms(), "action": reverse('book_example_editor_import')})

    raise Http404("Only ajax POST requests here")

def export_shapefile(request, shapefile_id):
    shapefile = get_object_or_404(Spatialdata, pk=shapefile_id) #get_object_or_404(Shapefile, pk=shapefile_id)
    # TODO: add some Celery thingy here
    response = tasks.call_export_data(shapefile.pk)
    return response

def delete_shapefile(request, shapefile_id):
    shapefile = get_object_or_404(Spatialdata, pk=shapefile_id) #get_object_or_404(Shapefile, pk=shapefile_id)
    shapefile.delete()
    return redirect(list_shapefiles)

def dummy(request, shapefile_id):
    return HttpResponse("")