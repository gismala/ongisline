from __future__ import absolute_import, unicode_literals
from celery import shared_task
from apps.bookexample.utils import import_data_trough_keyvalpairs, export_data, import_data_to_postgis, export_from_postgis
from apps.bookexample.models import Shapefile

@shared_task
def call_export_data(pk):
    return export_from_postgis(pk)

@shared_task
def call_import_data(fil, encoding):
    return import_data_trough_keyvalpairs(fil, encoding)

@shared_task
def import_to_postgis(fil, name):
    import_data_to_postgis(fil, name)