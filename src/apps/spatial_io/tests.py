"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
import os
import django

if os.name == 'nt': 
    django.setup() # Django requires an explicit setup() when running tests in PTVS

from django.test import TestCase
from django.db import connection

from django.contrib.gis.db import models

from apps.django_gdalextension import DataSource2
from django.contrib.gis.utils.ogrinspect import ogrinspect

from apps.spatial_io.models import _generate_custom_spatial_model, Spatialdata
from apps.spatial_io.utils import save_data_to_postgis, export_data_from_postgis

import time
import unittest
import tempfile
import tempfile
import shutil
import uuid
import logging
logger = logging.getLogger(__name__)

SINGLEPOLYGON = "e_L4131R_p.shp"
WORLDBORDERS = "TM_WORLD_BORDERS-0.3.shp"


# TODO: Configure your database in settings.py and sync before running tests.
#class SimpleTest(TestCase): <- change to this when needing django specifig tests
class SpatialIOTests(TestCase):
    fixtures = ['onepolygon.json']
    """Tests for the application views."""

    
    def setUp(self):
        self.dataDir = "data"
        self.formats = {
                        'CSV' : '.csv',
                        'ESRI Shapefile' : '.shp', 
                        'GeoJSON': '.json',
                        'GML' : '.gml',
                        'KML' : '.kml',
                        'MapInfo File': '.tab',
                        #'PDF' : '.pdf', not currently working?
                        'PGDump': '.sql',
                        }


    def tearDown(self):
        return super().tearDown()

    def initialize_test_data(self, datas, names):
        for i in range(len(datas)):
            if os.name == 'nt':
                try:
                    old_data = Spatialdata.objects.get(tablename=names[i])
                    old_data.delete()
                except:
                    logger.debug("Old test data did not exist")
            data = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.dataDir, datas[i])
            save_data_to_postgis(data, names[i], names[i], addToGeoserver=False)

    @unittest.skip("not ready yet")
    def test_model_creation(self):
        datas = [SINGLEPOLYGON, WORLDBORDERS]
        names = ["test_model_creation_single_polygon", "test_model_creation_worldborders"]
        ds = None
        
        # Import data to database
        self.initialize_test_data(datas, names)
        for i in range(len(datas)):
            try:
                test_table_name = names[i]
                ds = DataSource2(os.path.join(os.path.dirname(os.path.abspath(__file__)), self.dataDir, datas[i]))
                new_data = Spatialdata.objects.get(tablename=test_table_name)
                model = new_data.get_model()
                q = model.objects.all()
                self.assertEqual(len(q), len(ds[0]))
                del ds

                ds = None
                self.assertEqual(q[0].django_fk.pk, new_data.pk)
            except:
                logger.error("With data: " +datas[i])
                if ds:
                    del ds
                raise
        
    
    def test_spatial_export(self):
        datas = [SINGLEPOLYGON, WORLDBORDERS]
        names = ["test_spatial_export_single_polygon", "test_spatial_export_worldborders"]
        self.initialize_test_data(datas, names)

        try:
            format = None
            for table_name in names:
                new_data = Spatialdata.objects.get(tablename=table_name)
                
                for format_name, postfix in self.formats.items():
                    response = export_data_from_postgis(new_data.pk, format_name)
                    
        except:
            logger.error("With table: {} and format {}".format(table_name, format_name))
            raise

