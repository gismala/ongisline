from django.contrib.gis.db import models
from django.apps import apps
from django.db import connection

import logging
logger = logging.getLogger(__name__)


APP_LABEL = 'spatial_io'

class Spatialdata(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    tablename = models.CharField(unique=True, max_length=255)
    attrs = models.TextField(blank=True, null=True)
    # Feel free to add more fields

    def delete(self, *args, **kwargs):
        logger.debug("Deleting spatial data")
        # TODO: get rid of this dangerous method, use migrations instead
        super(Spatialdata, self).delete(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("DROP TABLE \""+self.tablename+"\";")


    def get_model(self):
        logger.debug("Getting the model")
        if self.tablename in apps.all_models[APP_LABEL].keys():
            return apps.all_models[APP_LABEL][self.tablename]
        else:
            return _generate_custom_spatial_model(self.tablename, dict(eval(self.attrs)))



def _generate_custom_spatial_model(table_name, attrs):
    """ Create a custom (dynamic) model class based on the given definition.
    """
    # you need to come up with a unique table name
    _db_table = table_name

    # you need to come up with a unique model name (used in model caching)
    _model_name = table_name

    # Remove any exist model definition from Django's cache
    try:
        del apps.all_models[APP_LABEL][_model_name.lower()]
    except KeyError:
        pass

    # The attributes values are stored as strings in db, eval them to get the right values
    for key, val in attrs.items():
        attrs[key] = eval(val)
    # Store a link to the definition for convenience
    #attrs['car_model_definition'] = car_model_definition

    # Create the relevant meta information
    class Meta:
        app_label = APP_LABEL
        db_table = _db_table
        managed = False
        verbose_name = 'Spatial model'
        verbose_name_plural = 'Spatial models'
        #ordering = ('my_field',)

    attrs['__module__'] = 'spatial_io.models'
    attrs['Meta'] = Meta
    attrs['django_fk'] = models.ForeignKey(Spatialdata, related_name='data')

    # Create the new model class
    model_class = type(_model_name, (models.Model,), attrs)
    apps.all_models[APP_LABEL][_model_name.lower()] = model_class
    #print(apps.all_models[APP_LABEL])
    return model_class