from django.contrib.gis.gdal import DataSource
from apps.django_gdalextension import ogr2ogr

from django.db import connection

import os
import logging
logger = logging.getLogger(__name__)




class Converter(object):

    def __init__(self, args):
        self.fname = args.get("file", "")
        self.format = args.get("format", self._find_format(self.fname))
        self.schema = args.get("schema", None)
        self.table_name = args.get("table_name", os.path.basename(self.fname.split(".")[0]))

    def convert(self, format, out_name, extra_ags=None):
        format_to = self._clean_format(format)
        args = ["", "-nlt", "PROMOTE_TO_MULTI", "-f", format_to, out_name, self.fname]
        args += ["-nln", self.table_name]
        if self.schema:
            args += ["-lco",  "SCHEMA={0}".format(self.schema)]
        if extra_ags:
            agts += extra_ags

        self._clean_args(args)
        ogr2ogr.main(args)

    def save_to_postgis(self):
        ouf_file = "tempdump.sql"
        try:
            self.convert("PGDump", ouf_file)
            with open(ouf_file, "r") as f:
                rows = f.readlines()
                rows[1] = rows[1].replace("CREATE SCHEMA", "CREATE SCHEMA IF NOT EXISTS")
                with connection.cursor() as cursor:
                    for row in rows:
                        cursor.execute(row)
        finally:
            if os.path.exists(ouf_file):
                os.remove(ouf_file)

    def _find_format(self, filename):
        ds = DataSource(filename)
        format = ds.driver.name
        del ds
        return format

    def _clean_format(self, format):
        #TODO: implement format check
        return format
    
    def _clean_args(self, args):
        pass


#def test_straight_to_postgis(self):
#        datas = [SINGLEPOLYGON, WORLDBORDERS]
#        for data in datas:
#            try:
#                logger.debug("Processing file " + data)
#                data = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.dataDir, data)
#                outfile = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.dataDir, "outdump.sql")
#                ogr2ogr.main(["", "-nlt", "PROMOTE_TO_MULTI", "-f", "PGDump", outfile, data, 
#                               "-nln", "test_table", "-lco",  "SCHEMA=test"])

#                with open(outfile, "r") as f:
#                    rows = f.readlines()
#                    rows[1] = rows[1].replace("CREATE SCHEMA", "CREATE SCHEMA IF NOT EXISTS")
#                    with connection.cursor() as cursor:
#                        for row in rows:
#                            cursor.execute(row)

#                if os.path.exists(outfile):
#                    os.remove(outfile)
#            except:
#                if os.path.exists(outfile):
#                    os.remove(outfile)
#                raise