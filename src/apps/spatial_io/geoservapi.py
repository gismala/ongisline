import requests
import os
import xml.etree.ElementTree as ET
import json

GEO_URL = "http://192.168.99.100:8080/geoserver/rest" if os.name == 'nt' else "http://geoserver:8080/geoserver/rest"
WORKSPACES = GEO_URL + "/workspaces"
NAMESPACES = GEO_URL + "/namespaces"
DATASTORES = WORKSPACES + "/{}/datastores"
AUTH = ('admin', 'geoserver')
HEADERS_GET_XML = {'Accept': 'text/xml'}
HEADERS_POST_XML = {'Content-type' : 'text/xml'}
HEADERS_GET_JSON = {'Accept': 'text/json'}
HEADERS_POST_JSON = {'Content-type' : 'text/json'}
HEADERS_ZIP = {'Content-type' : 'application/zip'}
HEADERS_SLD = {'Content-type': 'application/vnd.ogc.sld+xml'}


POSTGIS_XML ="""
<dataStore>
  <name>postgis_yhteys</name>
  <connectionParameters>
    <host>localhost</host>
    <port>5432</port>
    <database>nyc2</database>
    <user>postgres</user>
    <dbtype>postgis</dbtype>
  </connectionParameters>
</dataStore>
"""

def addLayer(layerName, title):
    url = DATASTORES.format("testi") + "/testi_store/featuretypes"
    #payload_json = {'featureType':[{'Name' : layerName, 'title' : title}]}
    #payload_json = json.dumps(payload_json)
    payload = '<featureType><name>{0}</name><title>{1}</title></featureType>'.format(layerName, title)
    r = requests.post(url, auth=AUTH, data=payload ,headers=HEADERS_POST_XML)
    print(r.text)
