from django import forms
from django.utils.safestring import mark_safe
import zipfile

FILESIZELIMIT = 5242880
CHARACTER_ENCODINGS = {"ASCII" :"ascii","LATIN-1": "latin1", "UTF-8" : "utf8"}

class importFilesForms(forms.Form):
    import_file = forms.FileField(
        required=False,
        label=mark_safe('<strong>Choose a file</strong><span class="box__dragndrop"> or drag it here</span>.'),
        widget=forms.ClearableFileInput(
        {'class' : 'box__file', 
         'multiple': True, 
         'data-multiple-caption' : "{count} files selected"})
        )

    def __init__(self, *args, **kwargs):
        self.encoding = kwargs.pop('type', None)
        self.format = kwargs.pop('format', None)
        super(importFilesForms, self).__init__(*args, **kwargs)

    def clean_import_file(self):
        cleaned_data = super(importFilesForms, self).clean()
        file = cleaned_data.get("import_file")
        if not file:
            raise forms.ValidationError("The file upload failed: file is null")
        if file.size > FILESIZELIMIT:
            raise forms.ValidationError("The size of the file {0} exceeds the file size limit".format(file.name))
        if zipfile.is_zipfile(file.file):
            self.format='zip'
            self.encoding = "utf8"
            with zipfile.ZipFile(file.file, 'r') as archive:
                namelist = archive.namelist()
                cpg = [fil for fil in namelist if fil.split('.')[-1] == "cpg"]
                cpg = cpg[0] if len(cpg) > 0 else None
                if cpg:
                    f = archive.read(cpg)
                    enc = f.decode("utf-8").upper()
                    if enc in CHARACTER_ENCODINGS.keys():
                        self.encoding = CHARACTER_ENCODINGS[enc]
        else:
            raise forms.ValidationError("The file upload failed: the file is not a zip file")
        return file
    