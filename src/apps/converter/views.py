from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_protect
from django.utils.datastructures import MultiValueDict
from django.urls import reverse

from .forms import importFilesForms

import logging
logger=logging.getLogger(__name__)

@csrf_protect
def converter_main(request):
    template = "converter/converter_main.html"
    if request.is_ajax() and request.method == 'POST':
        errorMsg = ""
        isSuccess = True
        logger.debug("Post and files: {0}, {1}".format(request.POST, request.FILES))
        # If the files are dropped, they are on the files[]. If choosed normally they are in import_file
        importFiles = request.FILES.getlist('import_file') + request.FILES.getlist('files[]')
        for importFile in importFiles:
            filesForm = importFilesForms(request.POST, MultiValueDict({'import_file' : [importFile]}))
            if not filesForm.is_valid():
                errorMsg = filesForm.errors['import_file']
                isSuccess = False
                break

        if isSuccess:
            logger.debug("upload is valid!")
        return JsonResponse({"upload_success": isSuccess, "upload_error" : errorMsg})

    # GET method
    return render(request, template, 
                  {"hello_to" : "World",
                   "form" : importFilesForms(),
                   "action" : reverse('converter_main')})


