from django.contrib.gis.gdal.prototypes import ds as vcapi
from django.contrib.gis.gdal.field import OFTInteger, OFTIntegerList, OFTReal, OFTRealList, OFTString, OFTStringList, OFTDate, OFTDateTime, OFTTime, OGRFieldTypes
from apps.django_gdalextension import ds2 as vcapi2
from django.utils.encoding import force_bytes
from django.contrib.gis.gdal.error import GDALException
from django.utils import six

def generate_index(feat_ptr, index, num_fields):
        if isinstance(index, six.integer_types) and index  < self.num_fields:
            index = index
        elif isinstance(index, six.string_types):
            index = vcapi.get_field_index(feat_ptr, force_bytes(index))
        else:
            raise GDALException("Unknown index or key value given")
        return index

def set_OGRFeature_Attribute(attr_type, index, value, feat_ptr,  num_fields, encoding):
    """
    Set the field attribute value based on field type
    """
    index = generate_index(feat_ptr, index, num_fields)

    #TODO: is this really fastest?
    if not isinstance(attr_type, six.integer_types):
        attr_type = [key for key, val in OGRFieldTypes.items() if val == attr_type]
    
    if not value or len(attr_type) == 0:
        vcapi2.unset_field(feat_ptr, index)
        return

    attr_type = attr_type[0]
    if attr_type == 0: #OFTInteger
        vcapi2.set_field_integer(feat_ptr, index, int(value))
    elif attr_type == 1: #OFTIntegerList
        ints = eval(value)
        vcapi2.set_field_integer_list(feat_ptr, index, len(ints), ints)
    elif attr_type == 2: #OFTReal
        vcapi2.set_field_double(feat_ptr, index, float(value))
    elif attr_type == 3: #OFTRealList
        floats = [eval(val) for val in eval(value)]
        vcapi2.set_field_double_list(feat_ptr, index, len(floats), floats)
    elif attr_type == 4: #OFTString
        vcapi2.set_field_string(feat_ptr, index, force_bytes(value, encoding))
    elif attr_type == 5: #OFTStringList
        values = [force_bytes(s, encoding) for s in eval(value)]
        vcapi2.set_field_string_list(feat_ptr, index, values)
    elif attr_type == 9: #OFTDate:
        year, month, day, tzone = value.split(",")
        vcapi2.set_field_datetime(feat_ptr, index, year, month, day, 0, 0, 0, tzone)
    elif attr_type == 10: #OFTTime
        hour,minute,second,tzone = value.split(",")
        vcapi2.set_field_datetime(feat_ptr, index, 0, 0, 0, hour, minute, second, tzone)
    elif attr_type == 11: #OFTDateTime
        year,month,day,hour,minute,second,tzone = value.split(",")
        vcapi2.set_field_datetime(feat_ptr, index, year, month, day, hour, minute, second, tzone)
    else:
        # TODO: implement other formats
        vcapi2.unset_field(feat_ptr, index)
        
