from django.contrib.gis.gdal.feature import Feature
from django.contrib.gis.gdal.prototypes import ds as vcapi

from apps.django_gdalextension import ds2 as vcapi2, utils
from django.utils.encoding import force_bytes, force_text

from django.contrib.gis.gdal.error import GDALException
from django.utils import six


class Feature2(Feature):
    def __del__(self):
        "Releases a reference to this object."
        try:
            if self._layer._ds._write:
                vcapi2.rewrite_feature(self._layer._ptr, self._ptr)
            vcapi.destroy_feature(self._ptr)
        except (AttributeError, TypeError):
            pass  # Some part might already have been garbage collected


    def get_value(self, index):
        index = utils.generate_index(self._ptr, index, self.num_fields)
        try:
            self._get_OGRFeature_Attribute(index)
        except:
            raise # Error handling

    def set_value(self, index, value):
        try:
            utils.set_OGRFeature_Attribute(self[index].type, index, 
                                           value, self._ptr, self.num_fields, self.encoding)
        except:
            raise # Error handling

    def _get_OGRFeature_Attribute(self, index):
        # TODO: implement this
        #if not feature.IsFieldSet(attr_name):
        #    return (True, None)
        attr_type = self[index].type
        needs_encoding = False
        if attr_type == 0: #OFTInteger
            value = self[index].as_string()
        elif attr_type == 1: #OFTIntegerList
            value = repr(self[index].value)
        elif attr_type == 2: #OFTReal
            value = self[index].as_double()
            value = "%*.*f" % (self[index].width, self[index].precision, value)
        elif attr_type == 3: #OFTRealList
            values = self[index].value
            str_values = []
            for value in values:
                str_values.append("%*.*f" % (attr.width,
                                attr.precision,
                                value))
            value = repr(str_Values)
        elif attr_type == 4: #OFTString
            value = self[index].as_string()
            needs_encoding = True
        elif attr_type == 5: #OFTStringList
            value = self(feature[index].value)
            needs_encoding = True
        elif attr_type == 9: #OFTDate:
            parts = self[index].as_datetime()
            year,month,day,hour,minute,second,tzone = parts
            value = "%d,%d,%d,%d" % (year,month,day,tzone)
        elif attr_type == 10: #OFTTime
            parts = self[index].as_datetime()
            year,month,day,hour,minute,second,tzone = parts
            value = "%d,%d,%d,%d" % (hour,minute,second,tzone)
        elif attr_type == 11: #OFTDateTime
            parts = self[index].as_datetime()
            year,month,day,hour,minute,second,tzone = parts
            value = "%d,%d,%d,%d,%d,%d,%d,%d" % (year,month,day,
                    hour,minute,
                    second,tzone)
        else:
            # TODO: implement other fields
            raise GDALException("Unsupported attribute type: " + str(attr_type))
        if needs_encoding:
            try:
                value = force_text(value, self.encoding)
            except UnicodeDecodeError:
                raise GDALException("Unable to decode value in " +
                        repr(index) + " attribute.&nbsp; " +
                        "Are you sure you're using the right " +
                        "character encoding?")
        return value