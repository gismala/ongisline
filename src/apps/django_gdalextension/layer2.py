from django.contrib.gis.gdal.layer import Layer
from django.contrib.gis.gdal.prototypes import ds as vcapi,  geom as gcapi
from django.contrib.gis.gdal.error import GDALException, OGRIndexError
from django.utils.encoding import force_bytes

from django.utils import six

from apps.django_gdalextension import ds2 as vcapi2, utils
from apps.django_gdalextension.feature2 import Feature2


class Layer2(Layer):
    def __getitem__(self, index):
        "Gets the Feature at the specified index."
        if isinstance(index, six.integer_types):
            # An integer index was given -- we cannot do a check based on the
            # number of features because the beginning and ending feature IDs
            # are not guaranteed to be 0 and len(layer)-1, respectively.
            if index < 0:
                raise OGRIndexError('Negative indices are not allowed on OGR Layers.')
            return self._make_feature(index)
        elif isinstance(index, slice):
            # A slice was given
            start, stop, stride = index.indices(self.num_feat)
            return [self._make_feature(fid) for fid in range(start, stop, stride)]
        else:
            raise TypeError('Integers and slices may only be used when indexing OGR Layers.')

    def __iter__(self):
        "Iterates over each Feature in the Layer."
        # ResetReading() must be called before iteration is to begin.
        vcapi.reset_reading(self._ptr)
        for i in range(self.num_feat):
            yield Feature2(vcapi.get_next_feature(self._ptr), self)

    def _make_feature(self, feat_id):
        """
        Helper routine for __getitem__ that constructs a Feature from the given
        Feature ID.  If the OGR Layer does not support random-access reading,
        then each feature of the layer will be incremented through until the
        a Feature is found matching the given feature ID.
        """
        if self._random_read:
            # If the Layer supports random reading, return.
            try:
                return Feature2(vcapi.get_feature(self.ptr, feat_id), self)
            except GDALException:
                pass
        else:
            # Random access isn't supported, have to increment through
            # each feature until the given feature ID is encountered.
            for feat in self:
                if feat.fid == feat_id:
                    return feat
        # Should have returned a Feature, raise an OGRIndexError.
        raise OGRIndexError('Invalid feature id: %s.' % feat_id)

    ## Layer methods
    def create_field(self, fld_input):
        """
        Create new vector field
        """

        num_fields = self.num_fields
        fld_name = fld_input.get("name", "field")[:10] # TODO: 10 limit only for shapefiles
        field_defn = vcapi2.create_field_defn(force_bytes(fld_input.get("name", "field")), int(fld_input.get("type", 4)))
        vcapi2.set_field_width(field_defn, fld_input.get("width", 32))
        vcapi2.set_field_precision(field_defn, fld_input.get("precision", 0)) # 0 for stringfields
        try:
            vcapi2.create_field(self._ptr, field_defn, 1)
        except GDALException:
            pass #TODO: error handling + OGRerror handling
        if not self.num_fields == num_fields +1:
            vcapi2.destroy_field(field_defn)
    
    def create_feature(self, feat_input):
        """
        Create new feature
        """
        feat_factory_ptr = vcapi2.create_feature_factory(vcapi.get_layer_defn(self._ptr))
        attr_query = feat_input.get("attributes", None)
        if attr_query:
            for i in range(len(attr_query)):
                utils.set_OGRFeature_Attribute(self.field_types[i], 
                                               attr_query[i].attribute.name, 
                                               attr_query[i].value, 
                                               feat_factory_ptr, 
                                               self.num_fields, self._ds.encoding) 

        # TODO: from other formats than json?
        geom_ptr = gcapi.from_json(force_bytes(feat_input.get("geom", "")))
        vcapi2.set_geometry_to_feature(feat_factory_ptr, geom_ptr)
        
        try:
            vcapi2.create_feature(self._ptr, feat_factory_ptr)
            vcapi.destroy_feature(feat_factory_ptr)
        except:
            pass # TODO: error handling