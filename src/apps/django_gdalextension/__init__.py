"""
TODO: doc
"""
from django.contrib.gis.gdal.envelope import Envelope
from django.contrib.gis.gdal.error import (  # NOQA
    GDALException, OGRException, OGRIndexError, SRSException, check_err,
)
from django.contrib.gis.gdal.geomtype import OGRGeomType  # NOQA

__all__ = [
    'HAS_GDAL',
]

# Attempting to import objects that depend on the GDAL library.  The
# HAS_GDAL flag will be set to True if the library is present on
# the system.
try:
    from django.contrib.gis.gdal.libgdal import gdal_version, gdal_full_version, GDAL_VERSION  # NOQA
    from apps.django_gdalextension.datasource2 import DataSource2
    from apps.django_gdalextension.layer2 import Layer2
    from apps.django_gdalextension.feature2 import Feature2

    HAS_GDAL = True
    __all__ += [
        'DataSource2', 'Layer2', 'Feature2', 'gdal_version', 'gdal_full_version', 'GDAL_VERSION'
    ]
except GDALException:
    HAS_GDAL = False