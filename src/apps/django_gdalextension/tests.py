"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
import os
import django
from django.contrib.gis.geos.geometry import GEOSGeometry



if os.name == 'nt': 
    django.setup() # Django requires an explicit setup() when running tests in PTVS

from django.test import TestCase
from django.forms.models import model_to_dict

from django.contrib.gis.gdal.field import OGRFieldTypes
from django.contrib.gis.gdal.srs import CoordTransform, SpatialReference
from django.contrib.gis.gdal.geometries import MultiLineString, MultiPolygon, Polygon, LineString

from django.contrib.gis.gdal import OGRGeomType, OGRGeometry
from django.db import connection

from apps.django_gdalextension import DataSource2
from apps.bookexample.models import Shapefile, Feature, Attribute, AttributeValue
from apps.bookexample.utils import export_data, calc_geometry_field, wrap_geos_Geometry, wrap_geometry

import time
import unittest
import tempfile
import tempfile
import shutil

import logging
logger = logging.getLogger(__name__)

SINGLEPOLYGON = "e_L4131R_p.shp"
WORLDBORDERS = "TM_WORLD_BORDERS-0.3.shp"

def wrap_geometry2(geometry):
    if isinstance(geometry, Polygon) or isinstance(geometry, MultiPolygon):
        geom = OGRGeometry("MULTIPOLYGON")
        geom.add(geometry)
        return geom.geos
    elif isinstance(geometry, LineString) or isinstance(geometry, MultiLineString):
        geom = OGRGeometry("MULTILINESTRING")
        geom.add(geometry)
        return geom.geos
    return geometry

def unwrap_geos_geometry(geometry):
    if isinstance(geometry, MultiPolygon) or isinstance(geometry, MultiLineString):
        if len(geometry) == 1:
            geometry = geometry[0]
    return geometry

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

# TODO: Configure your database in settings.py and sync before running tests.
#class SimpleTest(TestCase): <- change to this when needing django specifig tests
class Ogr2ogrTests(TestCase):
    fixtures = ['onepolygon.json']
    """Tests for the application views."""

    
    def setUp(self):
        self.dataDir = "data"
        self.formats = {
                        'CSV' : '.csv',
                        'ESRI Shapefile' : '.shp', 
                        'GeoJSON': '.json',
                        'GML' : '.gml',
                        'KML' : '.kml',
                        'MapInfo File': '.tab',
                        #'PDF' : '.pdf', not currently working?
                        'PGDump': '.sql',
                        }
        self.dst_dir = tempfile.mkdtemp()


    def tearDown(self):
        #shutil.rmtree(self.dst_dir)
        return super().tearDown()

    def test_saving_to_postgis(self):
        datas = [SINGLEPOLYGON, WORLDBORDERS]
        i = 0
        for data in datas:
            try:
                data = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.dataDir, data)
                ds = DataSource2(data)
                ds.save_to_postgis({"schema" : "public", "table_name" : "testdata{0}".format(i)})
                with connection.cursor() as cursor:
                    conn_str = "SELECT * from testschema.testdata" + str(i) + " LIMIT 2"
                    cursor.execute(conn_str);
                    res = dictfetchall(cursor)
                    logger.info(res)
            except:
                logger.error("With data: " +data)
                raise
            finally:
                if ds:
                    del ds
            i+=1

    def test_convert_to_formats(self):
        datas = [SINGLEPOLYGON, WORLDBORDERS]
        try:
            for i in range(len(datas)):
                data = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.dataDir, datas[i])
                ds = DataSource2(data)
                for format, postfix in self.formats.items():
                    out_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.dst_dir, "test_out{0}".format(i)+postfix)
                    ds.convert({"format" : format, "out_name":out_file})
                    self.assertTrue(os.path.exists(out_file), "The output wasn't created with data: {0} and format: {1}".format(data, format))
        except:
            logger.error("With data: {0} and format: {1}".format(data, format))
            shutil.rmtree(self.dst_dir)
            raise
        finally:
            if ds:
                del ds




# TODO: Configure your database in settings.py and sync before running tests.
#class SimpleTest(TestCase): <- change to this when needing django specifig tests
class VectorTests(TestCase):
    fixtures = ['onepolygon.json']
    """Tests for the application views."""

    
    def setUp(self):
        self.dataDir = "data"
        self.drivers = {'ESRI Shapefile' : '.shp'}
        #self.drivers = {
        #                'CSV' : '.csv',
        #                'ESRI Shapefile' : '.shp', 
        #                'GeoJSON': '.json',
        #                'GML' : '.gml',
        #                'KML' : '.kml',
        #                'MapInfo File': '.tab',
        #                'PDF' : '.pdf',
        #                'PGDump': '.sql',
        #                }
        self.testShape = Shapefile.objects.get(pk=17)
        self.testAttrs = list(self.testShape.attribute_set.all())
        self.testFeatures = list(self.testShape.feature_set.all())
        self.dst_dir = tempfile.mkdtemp()


    def tearDown(self):
        shutil.rmtree(self.dst_dir)
        return super().tearDown()

    # TODO: test memory leaks
    # TODO: comparison
    # TODO: test with every geometry type
    
    def test_datasource_creation_from_fixture(self):
        shapeDict = model_to_dict(self.testShape)
        basename = shapeDict['filename'].split('.')[0]
        geom_field = calc_geometry_field(shapeDict['geom_type'])

        for driver, suffix in sorted(self.drivers.items()):
            ds = layer = feat = None
            try:
                name = os.path.join(self.dst_dir, basename+suffix)
                shapeDict['filename'] = name
                ds = DataSource2(dict({"driver":driver}, **shapeDict), ds_create=True)
                layer = ds.create_layer("testlayer")
                self.assertEqual(layer._ds.encoding, "utf8", "The encoding wasn't created with driver: "+driver)
                self.assertEqual(ds.layer_count, 1, "The layer wasn't created for driver: "+driver)
                for attr in self.testAttrs:
                    layer.create_field(dict({"layer":layer}, **model_to_dict(attr)))
                self.assertGreaterEqual(layer.num_fields, len(self.testAttrs), "Some fields didn't create with driver: "+driver)

                i = 0
                for feature in self.testFeatures:
                    attrs = feature.attributevalue_set.all()
                    geom = getattr(feature, geom_field)
                    geom =unwrap_geos_geometry(geom)
                    layer.create_feature({"geom": geom.json, "attributes": attrs})

                    # TODO: test for this way also
                    #feat = layer[i]
                    #for attrVal in attrs:
                    #    feat.set_value(attrVal.attribute.name, attrVal.value)
                    #del feat #TODO: make more intuitive?
                    #i +=1
                    
                self.check_attribute_values(driver, layer)
               

                del ds
                del layer
            except:
                logger.error("Error occurred with driver: " + driver)
                del ds
                del layer
                del feat
                raise
        
    @unittest.skip("Not yet implemented")
    def test_datasource_creation_to_file(self):
        for driver, suffix in self.drivers.items():
            name = os.path.join(self.dst_dir, "testvector"+suffix)
            try:
                #Raises an error if fails
                ds = DataSource2({"filename":name, 
                           "driver":driver, 
                           "encoding":'utf8',
                           "srs_wkt": 4326}, ds_create=True)
                self.assertEqual(ds.name, name, "The vector name wasn't right, got: "+ds.name)
                
                layer = ds.create_layer("testlayer", OGRGeomType('Point'))
                self.assertEqual(ds.layer_count, 1, "The layer wasn't created for driver: "+driver)
                
                layer.create_field({"name": "tstFld", "layer" :layer, "type":4, "width":32}) # Precision, lookup for field type
                self.assertEqual(layer.num_fields, 1, "Field creation failed with driver: "+driver)
                del ds
                layer=None
            except:
                del ds
                layer=None
                raise
       
    @unittest.skip("Not yet implemented")
    def test_datasource_creation_to_folder(self):

        for driver in self.drivers.keys():
            name =  self.dst_dir
            try:
                pass
            except:
                raise

    def test_datasource_opening_and_saving(self):
        # Open the shapefile using OGR
        datas = [SINGLEPOLYGON]
        datasource = None
        layer = None
        for dataS in datas:
            try:
                datasource = DataSource2(os.path.join(os.path.dirname(os.path.abspath(__file__)), self.dataDir, dataS))
                layer = datasource[0]
            except :
                logger.exception("Error occurred while opening a datasource {0}".format(dataS))
                if datasource:
                    del datasource
                if layer:
                    del layer
                raise
 
            try:
                for layer in datasource:
                    geometry_name = layer.geom_type.name
                    geometry_field = calc_geometry_field(geometry_name)
                    # Store shapefile
                    src_spatial_ref = layer.srs
                    os.path.basename(datasource.name)
                    logger.info(os.path.basename(datasource.name))
                    shapefile = Shapefile.objects.create(filename=os.path.basename(datasource.name),
                                srs_wkt=str(src_spatial_ref),
                                geom_type=geometry_name,
                                encoding="utf8")

                    # Store attribute fields
                    attributes = []
                    for i in range(layer.num_fields):
                        field_type = [key for key, val in OGRFieldTypes.items() if val == layer.field_types[i]][0]
                        attr = Attribute.objects.create(shapefile=shapefile,
                            name=layer.fields[i],
                            type=field_type,
                            width=layer.field_widths[i],
                            precision=layer.field_precisions[i])
                        attributes.append(attr)

                    # Store features
                    transform = CoordTransform(src_spatial_ref, SpatialReference(4326))
                    for feat in layer:
                        feat.geom.transform(transform)
                    
                        args = {}
                        args['shapefile'] = shapefile
                        args[geometry_field] = wrap_geometry(feat.geom)
                        feature = Feature.objects.create(**args)

                        # Store attribute values
                        for attr in attributes:
                            attr_value = feat.get_value(attr.name)
                            attr_value = AttributeValue.objects.create(feature=feature,
                                        attribute=attr,
                                        value=attr_value)
            except:
                logger.exception("Error occurred with {0}".format(dataS))
                del datasource
                del layer
                raise

            del datasource
            del layer

    def test_bookexample_export(self):
        response = export_data(self.testShape)
        logger.info(response)


    def check_attribute_values(self, driver, layer):
        if driver in ['GML', 'GeoJSON', 'KML']:
            for feat in layer:
                attrnum = 0
                for i in range(feat.num_fields):
                    attrnum += 1 if len(repr(feat[i].value)) > 0 else 0
                self.assertGreater(atrrnum, 0, "{0} attributes got filled with driver {1}".format(attrnum, driver))
        elif driver not in ['MapInfo File', 'PGDump']:
            attrnum = 0
            for attr in self.testAttrs:
                value = layer.get_fields(attr.name)
                attrnum += 1 if len(value) > 0 and len(repr(value[0])) > 0 else 0
            self.assertGreater(attrnum, 0, "{0} attributes got filled with driver {1}".format(attrnum, driver))

    def compare(self, sh1, sh2):
        feats1 = sh1.feature_set.all()
        feats2 = sh2.feature_set.all()
        attrVals1 = sh1.attribute_set.all()
        attrVals2 = sh1.attribute_set.all()

        self.assertEqual(sh1.filename, sh2.filename)
        self.assertEqual(sh1.srs_wkt, sh2.srs_wkt)
        self.assertEqual(sh1.geom_type, sh2.geom_type)
        self.assertEqual(sh1.encoding, sh2.encoding)
        self.assertEqual(len(feats1), len(feats2))
        self.assertEqual(len(attrVals1), len(attrVals2))

        for i in range(len(attrVals1)):
            self.assertEqual(attrVals1[i].name, attrVals2[i].name)
            self.assertEqual(attrVals1[i].type, attrVals2[i].type)
            self.assertEqual(attrVals1[i].width, attrVals2[i].width)
            self.assertEqual(attrVals1[i].precision, attrVals2[i].precision)

        for i in range(len(feats1)):
            feat1 = feats1[i]
            feat2 = feats1[i]
            attrVals1 = feats1[i].attributevalue_set.all()
            attrVals2 = feats2[i].attributevalue_set.all()
            self.assertEqual(feat1.geom_point, feat2.geom_point)
            self.assertEqual(feat1.geom_multipoint, feat2.geom_multipoint)
            self.assertEqual(feat1.geom_multilinestring, feat2.geom_multilinestring)
            self.assertEqual(feat1.geom_multipolygon, feat2.geom_multipolygon)
            self.assertEqual(feat1.geom_geometrycollection, feat2.geom_geometrycollection)

            


def run_tests():
    suite = unittest.TestLoader().loadTestsFromTestCase(VectorTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
