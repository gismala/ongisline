"""
 This module houses the ctypes function prototypes for OGR DataSource
 related data structures. OGR_Dr_*, OGR_DS_*, OGR_L_*, OGR_F_*,
 OGR_Fld_* routines are relevant here.
"""
from ctypes import POINTER, c_char_p, c_int, c_double, c_char, c_void_p


from django.contrib.gis.gdal.libgdal import lgdal
from django.contrib.gis.gdal.prototypes.generation import (
    void_output, voidptr_output,
)

c_int_p = POINTER(c_int)  # shortcut type
c_double_p = POINTER(c_double)  # shortcut type

# Datasource
create_vector_ds = voidptr_output(lgdal.OGR_Dr_CreateDataSource, [c_void_p, c_char_p,  c_void_p])
create_layer = voidptr_output(lgdal.OGR_DS_CreateLayer, [c_void_p, c_char_p,  c_void_p, c_int, c_void_p])

# Field Routines
create_field_defn = voidptr_output(lgdal.OGR_Fld_Create, [c_char_p, c_int])
set_field_width = void_output(lgdal.OGR_Fld_SetWidth, [c_void_p, c_int], errcheck=False)
set_field_precision = void_output(lgdal.OGR_Fld_SetPrecision, [c_void_p, c_int], errcheck=False)
destroy_field = voidptr_output(lgdal.OGR_Fld_Destroy, [c_void_p], errcheck=False)

# Feature Routines
set_geometry_to_feature = void_output(lgdal.OGR_F_SetGeometry, [c_void_p, c_void_p], errcheck=False)
set_geometry_to_feature_directly = void_output(lgdal.OGR_F_SetGeometryDirectly 	, [c_void_p, c_void_p], errcheck=False)
rewrite_feature = voidptr_output(lgdal.OGR_L_SetFeature, [c_void_p, c_void_p], errcheck=False)

set_field_datetime = void_output(lgdal.OGR_F_SetFieldDateTime, [c_void_p, c_int, c_int, c_int, c_int, c_int, c_int, c_int, c_int], errcheck=False)
set_field_double = void_output(lgdal.OGR_F_SetFieldDouble, [c_void_p, c_int, c_double], errcheck=False)
set_field_double_list = void_output(lgdal.OGR_F_SetFieldDoubleList, [c_void_p, c_int, c_int, c_double_p], errcheck=False)
set_field_integer = void_output(lgdal.OGR_F_SetFieldInteger, [c_void_p, c_int, c_int], errcheck=False)
set_field_integer_list = void_output(lgdal.OGR_F_SetFieldIntegerList, [c_void_p, c_int, c_int, c_int_p], errcheck=False)
set_field_string = void_output(lgdal.OGR_F_SetFieldString, [c_void_p, c_int, c_char_p], errcheck=False) 
set_field_string_list = void_output(lgdal.OGR_F_SetFieldStringList, [c_void_p, c_int, POINTER(c_char_p)], errcheck=False) 
unset_field = void_output(lgdal.OGR_F_UnsetField, [c_void_p, c_int], errcheck=False)

# Geometry Routines
create_geometry = voidptr_output(lgdal.OGR_G_CreateGeometry, [c_int])
set_point_2D = void_output(lgdal.OGR_G_SetPoint_2D, [c_void_p, c_int, c_double, c_double], errcheck=False)
destroy_geometry = void_output(lgdal.OGR_G_DestroyGeometry, [c_void_p], errcheck=False)

# Layer Routines
create_field = void_output(lgdal.OGR_L_CreateField, [c_void_p, c_void_p, c_char], errcheck=False)
create_feature_factory = voidptr_output(lgdal.OGR_F_Create, [c_void_p])
create_feature = void_output(lgdal.OGR_L_CreateFeature, [c_void_p, c_void_p], errcheck=False)