from django.apps import AppConfig


class django_gdalextensionConfig(AppConfig):
    name = 'django_gdalextension'
