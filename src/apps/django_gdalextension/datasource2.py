import os, json
from django.contrib.gis.gdal.driver import Driver
from django.contrib.gis.gdal.datasource import DataSource
from django.contrib.gis.gdal.srs import SpatialReference
from django.contrib.gis.gdal.geomtype import OGRGeomType
from django.contrib.gis.gdal.prototypes import ds as vcapi

from apps.django_gdalextension import ds2 as vcapi2
from apps.django_gdalextension.layer2 import Layer2
from apps.django_gdalextension import ogr2ogr

from django.utils.encoding import force_bytes

from django.contrib.gis.gdal.error import GDALException, OGRIndexError
from django.utils import six
from django.db import connection
from django.db import migrations as db
from django.conf import settings
from ctypes import c_void_p

import tempfile

class DataSource2(DataSource):
    def __init__(self, ds_input, ds_create=False, ds_driver=False, write=False, encoding='utf-8'):
        if not ds_input:
            self.encoding = encoding
            return
        if not ds_create:
            # Regular DataSource
            super().__init__(ds_input, ds_driver, write, encoding)
        else:
            # About to create a datasource
            Driver.ensure_registered()
            # Preprocess json inputs. This converts json strings to dictionaries,
            # which are parsed below the same way as direct dictionary inputs.
            if isinstance(ds_input, six.string_types) and json_regex.match(ds_input):
                ds_input = json.loads(ds_input)

            # If input is a valid file path, try setting file as source.
            if isinstance(ds_input, six.string_types):
                if not os.path.exists(ds_input):
                    raise GDALException('Unable to read raster source input "{}"'.format(ds_input))
                try:
                    # GDALOpen will auto-detect the data source type.
                    self.datasource = vcapi.open_ds(force_bytes(ds_input), self._write)
                except GDALException as err:
                    raise GDALException('Could not open the datasource at "{}" ({}).'.format(ds_input, err))
            elif isinstance(ds_input, dict):
                # A new raster needs to be created in write mode
                self._write = 1

                # Create driver (Shapefile by default)
                driver = Driver(ds_input.get('driver', 'ESRI Shapefile'))

                # For out of memory drivers, check filename argument
                if driver.name != 'MEM' and 'filename' not in ds_input:
                    raise GDALException('Specify name for creation of raster with driver "{}".'.format(driver.name))
            
                ## Check if layername was specified
                #if 'layername' not in ds_input:
                #    raise GDALException('Specify layer name for JSON or dict input.')

                # Check if srid was specified
                if 'srs_wkt' not in ds_input:
                    raise GDALException('Specify srid for JSON or dict input.')

                ### Create OGR Datasource
                self.ptr = vcapi2.create_vector_ds(driver._ptr, 
                                               force_bytes(ds_input.get('filename', '')),
                                               None) # Replace None with driver specifig options
                # Set SRID
                self.srs = SpatialReference(ds_input.get('srs_wkt', '')) # TODO: set default

                self.geom_type = ds_input.get('geom_type', None)
            elif isinstance(ds_input, c_void_p):
                # Instantiate the object using an existing pointer to a ogr ds
                # TODO: Check this out
                self.datasource = ds_input
            else:
                raise GDALException('Invalid data source input type: "{}".'.format(type(ds_input)))

            # Call the base init
            super().__init__(self.ptr, ds_driver=driver._ptr, write=True, encoding=ds_input.get("encoding", 'utf-8'))
            


    def __getitem__(self, index):
        "Allows use of the index [] operator to get a layer at the index."
        if isinstance(index, six.string_types):
            l = vcapi.get_layer_by_name(self.ptr, force_bytes(index))
            if not l:
                raise OGRIndexError('invalid OGR Layer name given: "%s"' % index)
        elif isinstance(index, int):
            if index < 0 or index >= self.layer_count:
                raise OGRIndexError('index out of range')
            l = vcapi.get_layer(self._ptr, index)
        else:
            raise TypeError('Invalid index type: %s' % type(index))
        return Layer2(l, self)

     # #### Vector Methods ####
    def create_layer(self, layer_name, geom_type=None):
        """
        Create new vector layer
        """
        # The layer_type can be in any format acceptable by OGRGeomType
        # TODO: correct version for GDAL>=2.0  http://www.gdal.org/ogr_apitut.html
        # TODO: srs if not assigned
        geom_type = geom_type  if geom_type else self.geom_type
        geom_type = OGRGeomType(geom_type)
        layer = vcapi2.create_layer(self._ptr, force_bytes(layer_name), 
                                   self.srs._ptr, geom_type.num, None) # TODO: Replace None with driver specific content
        if not layer:
            raise GDALException("The layer creation failed")
        return Layer2(layer, self)

    def convert(self, args, fromDatabase=False):  
        format_to = self._clean_format(args.get("format", ""))
        out_name = args.get("out_name")
        schema = args.get("schema")
        pg_info = args.get("PG")
        self.table_name = args.get("table_name")


        ogr_args = ["", "-nlt", "PROMOTE_TO_MULTI", "-f", format_to]
        if pg_info and not fromDatabase:
            ogr_args.append(pg_info)
        if out_name:
            ogr_args.append(out_name)
        if pg_info and fromDatabase:
            ogr_args.append(pg_info)
        if fromDatabase:
            ogr_args.append(self.table_name)
        else:
            ogr_args.append(self.name)
        if self.table_name:
            ogr_args += ["-nln", self.table_name]
        if schema:
            ogr_args += ["-lco",  "SCHEMA={0}".format(schema)]
        
        self._clean_args(ogr_args)

        success = ogr2ogr.main(ogr_args)
        if not success:
            print(ogr_args)
            raise GDALException("Unable to perform the convert operation")
        success = None

    def save_to_postgis(self, args, django_model_tablename=None, django_model_pk=None):
        out_file = tempfile.NamedTemporaryFile(suffix='.sql').name #"tempdump.sql"
        is_dump = args.get("format") == "PGDump"
        is_postgis = args.get("format") == "PostgreSQL"
        if is_postgis:
            args["PG"] ="PG:host={HOST} port={PORT} user={USER} dbname={NAME} password={PASSWORD}".format(**settings.DATABASES['default'])
        if is_dump:
            args["out_name"] = out_file
        
        try:
            self.convert(args)
            if is_dump:
                with open(out_file, "r") as f:
                    rows = f.readlines()
                
                    rows[1] = rows[1].replace("CREATE SCHEMA", "CREATE SCHEMA IF NOT EXISTS")
                    with connection.cursor() as cursor:
                        [cursor.execute(row) for row in rows]

            if is_postgis or is_dump:
                # TODO: Find out how to not disturb django db connection while using ogr2ogr postgres driver...
                with connection.cursor() as cursor:
                    if django_model_tablename and django_model_pk:
                        cursor.execute("ALTER TABLE \""+self.table_name+"\" ADD COLUMN django_fk_id INTEGER;")
                        # TODO: maybe implement the -geomfield to ogr2ogr...
                        cursor.execute("ALTER TABLE \""+self.table_name+"\" RENAME COLUMN wkb_geometry TO geom;")
                        cursor.execute("ALTER TABLE \""+self.table_name+"\" ADD CONSTRAINT \""+self.table_name+"_fk\" FOREIGN KEY (django_fk_id) REFERENCES "+django_model_tablename+"(id) ON DELETE CASCADE;")
                        cursor.execute("UPDATE \""+self.table_name+"\" SET django_fk_id=%s;", [django_model_pk])
        finally:
            if os.path.exists(out_file):
                os.remove(out_file)


    def export_from_postgis(self, args):
        args["PG"] ="PG:host={HOST} port={PORT} user={USER} dbname={NAME} password={PASSWORD}".format(**settings.DATABASES['default'])
        self.convert(args, True)

    def alter_postgis_table(self, command):
        command_list = ["Alter table "+self.table_name+" "+command] if isinstance(command, six.string_types) else command
        with connection.cursor() as cursor:
            for row in command_list:
                cursor.execute(row)

    def _find_format(self, filename):
        ds = DataSource(filename)
        format = ds.driver.name
        del ds
        return format

    def _clean_format(self, format):
        #TODO: implement format check
        return format
    
    def _clean_args(self, args):
        pass
