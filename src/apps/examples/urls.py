from django.conf.urls import url
from . import views
from .models import WeatherStation

urlpatterns = [
    url(r'^weather/([0-9]+)$', views.weatherStations, name='examples_weather'),
    url(r'^weather/geoms.geojson$', views.weather_rest, name='examples_weather_rest'),
]