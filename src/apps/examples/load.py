import csv
import os
from django.contrib.gis.geos import Point

from .models import WeatherStation

def dms2dec(value):
    """
    Degres Minutes Seconds to Decimal degres
    """
    degres, minutes, seconds = value.split()
    seconds, direction = seconds[:-1], seconds[-1]
    dec = float(degres) + float(minutes)/60 + float(seconds)/3600
    if direction in ('S', 'W'):
        return -dec
    return dec

csv_file = os.path.join(os.path.dirname(__file__), 'data', 'weather_data.csv')

def loadWeatherData():
    reader = csv.DictReader(open(csv_file, 'r'), delimiter=";")
    for line in reader:
        lng = float(line.pop('Longitude'))
        lat = float(line.pop('Latitude'))
        wmoid = int(line.pop('StationId'))
        name = line.pop('StationName').title()
        print([lng, lat, wmoid, name])
        WeatherStation(wmoid=wmoid, name=name, geom=Point(lng, lat)).save()

