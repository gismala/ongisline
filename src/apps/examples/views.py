from django.shortcuts import render
from django.http import HttpResponse
from .models import WeatherStation
from django.views.decorators.gzip  import gzip_page
import json

import logging
logger=logging.getLogger(__name__)

def get_data():
    data = {"type": "FeatureCollection",
            "features" : [json.loads(value["geom"].json) for value in WeatherStation.objects.all().values("geom")],
            "crs": 
                {
                    "type" : "link",
                    "properties" :
                    {
                        "href": "http://spatialreference.org/ref/epsg/4326/",
                        "type" : "proj4"
                    }
                }
            }
    return json.dumps(data)

def weatherStations(request, method):
    context = {}
    if method=="1":
        context["data"] = get_data()
    elif method=="2":
        context["rest_url"] = "examples_weather_rest"
    elif method=="3":
        context["data_url"] = "examples_weather_data"

    return render(request, "examples/weather_map.html", context)

@gzip_page
def weather_rest(request):
    data = get_data()
    return HttpResponse(data, content_type="application/json")