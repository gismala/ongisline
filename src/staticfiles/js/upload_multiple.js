﻿// Applied from https://css-tricks.com/drag-and-drop-file-uploading/
var fileSizeLimit = 5242880;

$(document).ready(function () {
    // Check if the browser supports the drag'n drop upload
    var isAdvancedUpload = function () {
        var div = document.createElement('div');
        return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
    }();

    var isFileSuitable = function (file) {
        return file.size < fileSizeLimit; //&& (file.type.match("image.*")
    };

    $('.box').each(function () {

        var $form = $('.box');
        var $input = $form.find('input[type="file"]'),
        $errorMsg = $form.find('.box__error'),
        $label = $form.find('label'),
        showFiles = function (files) {
            $label.text(files.length > 1 ? ($input.attr('data-multiple-caption') || '').replace('{count}', files.length) : files[0].name);
        };

        // letting the server side to know we are going to make an Ajax request
        $form.append('<input type="hidden" name="ajax" value="1" />');

        // Show files after upload
        $input.on('change', function (e) {
            showFiles(e.target.files);
        });

        if (isAdvancedUpload) {
            $form.addClass('has-advanced-upload');
        }
        if (isAdvancedUpload) {

            var droppedFiles = false;

            $form.on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
                e.preventDefault();
                e.stopPropagation();
            })
            .on('dragover dragenter', function () {
                $form.addClass('is-dragover');
            })
            .on('dragleave dragend drop', function () {
                $form.removeClass('is-dragover');
            })
            .on('drop', function (e) {
                if (droppedFiles) droppedFiles = null;
                droppedFiles = e.originalEvent.dataTransfer.files;
                // TODO: implement checks for old Ajax also
                droppedFiles = $.grep(droppedFiles, function (file, i) {
                    if (!isFileSuitable(file)) {
                        return false; // Remove file
                    }
                    return true; // keep the element in the array
                });
                showFiles(droppedFiles);
            });
        }

        $form.on('submit', function (e) {
            if ($form.hasClass('is-uploading')) return false;

            $form.addClass('is-uploading').removeClass('is-error');

            if (isAdvancedUpload) {
                // ajax for modern browsers
                e.preventDefault();
                var ajaxData = new FormData($form.get(0));

                // Loop trough the dropped files
                if (droppedFiles) {
                    $.each(droppedFiles, function (i, file) {
                        ajaxData.append($input.attr('name'), file);
                    });
                }

                $.ajax({
                    url: $form.attr('action'),
                    type: $form.attr('method'),
                    data: ajaxData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    complete: function () {
                        console.log("complete");
                        $form.removeClass('is-uploading');
                        setTimeout(function () {
                            var formDiv = $(".form_loaded");
                            formDiv.empty();
                            formDiv.removeClass("form_loaded");
                            formDiv.addClass("import_form");

                        }, 2000);
                    },
                    success: function (data) {
                        console.log(data);
                        $form.addClass(data.upload_success == true ? 'is-success' : 'is-error');
                        $form.removeClass('is-uploading');
                        console.log("success");
                        if (!data.upload_success) $errorMsg.text(data.upload_error);
                    },
                    error: function () {
                        $form.addClass(data.upload_success != true ? 'is-success' : 'is-error');
                        alert('Error while uploading the data!');
                    }
                });
            } else {
                // ajax for legacy browsers -- Needs more studying
                var iframeName = 'uploadiframe' + new Date().getTime();
                $iframe = $('<iframe name="' + iframeName + '" style="display: none;"></iframe>');

                $('body').append($iframe);
                $form.attr('target', iframeName);

                $iframe.one('load', function () {
                    var data = JSON.parse($iframe.contents().find('body').text());
                    $form
                      .removeClass('is-uploading')
                      .addClass(data.success == true ? 'is-success' : 'is-error')
                      .removeAttr('target');
                    if (!data.success) $errorMsg.text(data.error);
                    $form.removeAttr('target');
                    $iframe.remove();
                });
            }
        });
    });

});
