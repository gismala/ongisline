import os
from ongisline.settings import BASE_DIR, DEBUG

logging_dict = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)-7s %(asctime)s %(name)s.%(funcName)s():%(lineno)-6s %(message)s',
             'datefmt':'%d.%m.%Y %H:%M:%S',
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'null': { #Handler for the situations when the logging wouldn't otherwise function
            'level':'DEBUG',
            'class':'logging.NullHandler',
        },
        'file': {
            'level': 'WARNING',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'logs', 'ongisline.log'),
            'maxBytes': 16777216, # 16megabytes
            'formatter': 'verbose'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'email_backend': 'django.core.mail.backends.console.EmailBackend', #Change this to real backend if serious developing
            'include_html': True,
        }
    },
    'loggers': {
        # Own apps in the "apps" folder
        'apps': {
            'handlers': ['null','console','file', 'mail_admins'],
            'level': 'DEBUG' if DEBUG else 'INFO',
            'propagate': True,
        },
    }
}