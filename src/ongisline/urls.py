"""ongisline URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import os
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib.gis import admin
from django.conf import settings
from django.views.generic import RedirectView


from ongisline.settings import CONVERTER_APP_URL, EXAMPLES_APP_URL, BOOKEXAMPLE_APP_URL

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', RedirectView.as_view(pattern_name='book_example_list_shapefiles', permanent=True)),
    url(r'^{0}/'.format(CONVERTER_APP_URL), include('apps.converter.urls')),
    url(r'^{0}/'.format(EXAMPLES_APP_URL), include('apps.examples.urls')),
    url(r'^{0}/'.format(BOOKEXAMPLE_APP_URL), include('apps.bookexample.urls')),
]

if "DYNO" not in os.environ:
    urlpatterns+=static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)